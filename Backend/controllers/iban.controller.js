const Models = require(__models);
const Hlp = require(__helpers + 'userHelpers');

function getIbans(req, res) {
    Models.IBAN.findAll().then((result) => {
        res.status(200).json(result)
    }).catch((Err) => {
        res.status(500).json({
            message: "Something Went Wrong ! ",
            error: Err
        })
    });
}

function getIban(req, res){
    if(!Hlp.hasAllParams(req.params, ['id'])){
        return res.status(400).json({
            message: "BAD REQUEST: 'id' not found !"
        })
    }

    Models.IBAN.findByPk(req.params.id).then(result => {
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).json({
                message: "Iban Not Found!"
            })
        }
    }).catch(Err => {
        res.status(500).json({
            message: "Something Went Wrong ! ",
            error: Err
        })
    });
}

function deleteIban(req, res){
    if(!Hlp.hasAllParams(req.params, ['id'])){
        return res.status(400).json({
            message: "BAD REQUEST: 'id' not found !"
        })
    }

    Models.IBAN.findByPk(req.params.id).then(result => {
        if (result) {
            Models.IBAN.destroy(
                {
                    where: { id: req.params.id }
                }
            ).then(result => {
                res.status(200).json({
                    message: "Deleted successfully ! ",
                    result: result
                })
            }).catch(err => {
                res.status(500).json({
                    message: "something went wrong ! ",
                    error: err
                })
            });
        } else {
            res.status(404).json({
                message: "Iban Not Found!"
            })
        }
    }).catch(Err => {
        res.status(500).json({
            message: "Something Went Wrong ! ",
            error: Err
        })
    });
}


function enregister(req, res) {

    let iban = Hlp.fetchIbanFromRequest(req.body)

    if(typeof iban == 'undefined'){
        return res.status(400).json({
            message: "BAD REQUEST: not enugh parameters!"
        })
    }
    Models.IBAN.findOne({
        where: { numeroIban: req.body.numeroIban}
    }).then(result => {
        if (result) {
            res.status(409).json({
                message: "Iban already Exist"
            })
        } else {

            ValidationResponse = Hlp.ValidateIbanFormat(iban)

            if (ValidationResponse !== true) return res.status(400).json({ message: "Invalide Format !", error: ValidationResponse });

                    Models.IBAN.create(iban).then(result => {
                        res.status(201).json({
                            iban: iban
                        })
                    }).catch(error => {
                        res.status(201).json({
                            message: "Something went Wrong !",
                            error: error
                        })
                    });
        }
    }).catch(err => {
        res.status(500).json({
            message: "Something went wrong",
            error: err
        })
    });
}



function updateIban(req, res) {

    let Ibans = Hlp.fetchIbanFromRequest(req.body)

    if(!Hlp.hasAllParams(req.params, ['id']) || typeof Ibans == 'undefined'){
        return res.status(400).json({
            message: "BAD REQUEST: not enugh parameters!"
        })
    }

    let id = req.params.id
    
    Models.IBAN.findByPk(id).then(result => {
        if (result) {

            Models.IBAN.update(Ibans, { 
                where: { id: id } 
            }).then(result => {
                res.status(200).json(result)
            }).catch(err => {
                res.status(500).json({
                    message: "Something went wrong",
                    error: err
                })
            });


        } else {
            res.status(404).json({
                message: "Iban Not Found!"
            })
        }
    }).catch(err => {
        res.status(500).json({
            message: "Something went wrong ----",
            error: err
        })
    });

}

module.exports = {
    getIbans: getIbans,
    getIban: getIban,
    deleteIban: deleteIban,
    updateIban:updateIban,
    enregister: enregister,
}
