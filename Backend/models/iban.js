'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class IBAN extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.associateToUser = this.belongsTo(models.User, {
        foreignKey: 'idUsers',
        onDelete: 'CASCADE'
      })

    }
  };
  IBAN.init({
    numeroIban: DataTypes.STRING,
    idUsers: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'IBAN',
  });
  return IBAN;
};