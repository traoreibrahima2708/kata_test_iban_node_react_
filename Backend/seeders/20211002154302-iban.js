'use strict';

const faker = require('faker');

const ibanList =  [];

for (let i = 0; i < 1000; i++) {
  ibanList.push({
      numeroIban: faker.random.number({min: 1000, max: 2000}),
      idUsers: faker.random.number({min: 1000, max: 2000}),
      createdAt : faker.date.past(2) ,
      updatedAt : faker.date.past(2) ,
    })
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
      return queryInterface.bulkInsert('IBANs', ibanList, {});

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete('IBANs', null, {})

  }
};
