
const express = require('express');
const router = express.Router();
const Controller = require(__controllers + 'iban.controller')
const authMiddleWare = require(__middleWares + 'userMiddleWare');

function adminMiddlWare(req, res, next) {
    if(!req.userData.isAdmin)
        res.status(401).json({
            message: 'Unauthorized Request !'
        })
    else next()
}

router.get('/all',  Controller.getIbans)
router.get('/:id',  Controller.getIban)
router.get('/del/:id', authMiddleWare.checkAuth, Controller.deleteIban)
router.post('/add', authMiddleWare.checkAuth,  Controller.enregister)
router.patch('/:id/edit', authMiddleWare.checkAuth, adminMiddlWare, Controller.updateIban)


module.exports = router


