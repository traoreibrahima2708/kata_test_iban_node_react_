import React, { Component } from "react";


class Ibans extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            error:null,
            isLoaded:false,
            items: []

        };
    }
    componentDidMount(){
        fetch("/iban/all")
        .then(res=>res.json())
        .then(
            (result)=>{
               
                this.setState({
                    isLoaded:true,
                    items:result,
                  
                });
                console.log(result);
            },
            (error)=>{
                this.setState({
                    isLoaded:true,
                    error
                });
            }
        )
    }
    state = {

        ibans: {}
    };

    handleChange = event => {
        console.log(event.target.value);
        this.setState({ [event.target.name]: event.target.value });
    }
    sendIban = event => {
        fetch('/iban/add', {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.numeroIban),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);

            })
            .catch((error) => {
                console.error('Error:', error);
            });

    }
    //Get
    getIbans = () => {
        fetch('/iban/all')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ ibans: data })
            })
            .catch((error) => {
                console.error('Error:', error);
            });;
    }

    //Appels
    // componentDidMount() {
    //     this.getIbans()
    // }

    render() {
        const {
            error, isLoaded, items
        } =this.state;

        if(error){
            return <div>
                Error:{error.message}
            </div>
        }else if(!isLoaded){
            return <div> Loading..</div>
        }else{
            return (
                <ul>
                    {items.map(item=>{
                        <li key={item.numeroIban}>
                            {item.numeroIban}
                        </li>
                    })}
                </ul>
            )
        }

        // return (
        //     <div>
        //         {
                   
        //                 <div>
        //                     <h2>YES</h2>
                            
        //                     <table>
        //                         <thead>
        //                             <tr>
        //                                 <th colspan="2">The table header</th>
        //                             </tr>
        //                         </thead>
        //                         <tbody>
        //                             <tr>
        //                                 <td></td>
        //                                 <td>with two columns</td>
        //                             </tr>
        //                         </tbody>
        //                     </table>
        //                 </div>

                    
        //         }
        //         <div>

        //         </div>
        //     </div>



        // )
    }
}
export default Ibans