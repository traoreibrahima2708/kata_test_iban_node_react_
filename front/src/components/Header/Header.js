import React from "react"
import './Header.css';
// const Header = (progs) => {
class Header extends React.Component{
  render(){
    console.log(this.props);
    return (
      <header className="Header-header">
  
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
          <a className="navbar-brand" href="#">Hello Dear</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
  
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Name: {this.props.name} City: {this.props.city}</a>
              </li>
            </ul>
  
          </div>
        </nav>
  
  
      </header>
  
  
    );
  }
 
}


export default Header